<?php
namespace Bender\dre_ClearTemp\core;

class dre_cleartmp_oxshopcontrol extends dre_cleartmp_oxshopcontrol_parent{

    protected function _runOnce()
    {
        $config = \OxidEsales\Eshop\Core\Registry::getConfig();
        $drecleartmpDevMode = $config->getShopConfVar('drecleartmpDevMode', \OxidEsales\Eshop\Core\Registry::getConfig()->getShopId(), 'module:dre_cleartemp');
        if ($drecleartmpDevMode && !$config->isProductiveMode()) {
            \OxidEsales\Eshop\Core\Registry::getUtils()->resetLanguageCache();
            $tmpDirectory = realpath($config->getShopConfVar('sCompileDir'));
            $aFiles = glob($tmpDirectory . '/smarty/*.php');
            $aFiles = array_merge($aFiles, glob($tmpDirectory . '/ocb_cache/*.json'));
            $aFiles = array_merge($aFiles, glob($tmpDirectory . '/dre_cache/*'));
            $aFiles = array_merge($aFiles, glob($tmpDirectory . '/*.txt'));
            $aFiles = array_merge($aFiles, glob($tmpDirectory . '/*.php'));
            if (count($aFiles) > 0) {
                foreach ($aFiles as $file) {
                    @unlink($file);
                }
            }
        }
        parent::_runOnce();
    }
}
