<?php
/**
 * Metadata version
*/
$sMetadataVersion = '2.0';
/**
 * Module information
 *	Dieses Modul erweitert den Head im Admin um den Cache zu leeren.
 */
$aModule = array(
    'id'           => 'dre_cleartemp',
    'title'        =>  '<img src="../modules/bender/dre_cleartmp/out/img/favicon.ico" title="Bender Clear Temp Modul">ody Clear Temp',
    'description'  => array(
        'de' => 'Modul erweitert den Head im Admin um den Cache zu leeren.',
        'en' => 'Modul for cleaning the cache.',
    ),
    'thumbnail'     => 'out/img/logo_bodynova.png',
    'version'       => '2.0.0',
    'author'        => 'Andre Bender',
    'url'           => 'https://bodynova.de',
    'email'         => 'a.bender@bodynova.de',
    'extend'	    => [
	   \OxidEsales\Eshop\Application\Controller\Admin\NavigationController::class
        => \Bender\dre_ClearTemp\Controller\Admin\NavigationController::class,
        \OxidEsales\Eshop\Core\ShopControl::class
        => \Bender\dre_ClearTemp\core\dre_cleartmp_oxshopcontrol::class
	],
    'settings'    => [
        ['group' => 'drecleartmp_main', 'name' => 'drecleartmpDevMode', 'type' => 'bool', 'value' => false],
        ['group' => 'drecleartmp_main', 'name' => 'drecleartmpPictureClear', 'type' => 'bool', 'value' => false],
        ['group' => 'drecleartmp_main', 'name' => 'drecleartmpRemoteHosts', 'type' => 'arr']
    ],
    'blocks'      => [
        [
            'template' => 'header.tpl',
            'block'    => 'admin_header_links',
            'file'     => '/views/admin/blocks/header_admin_header_links.tpl',
        ],
        [
            'template' => 'dre_startmenu.tpl',
            'block'    => 'admin_header_links',
            'file'     => '/views/admin/blocks/header_admin_header_links.tpl',
        ],
    ],
);
