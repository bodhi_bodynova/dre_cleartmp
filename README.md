# dre_clearTmp

Den Temp Ordner leeren aus dem Oxid Admin heraus.

#### Das Repository mittels composer installieren:

Composer configurieren, damit er an der richtigen Stelle sucht:
```php
composer config repositories.hidebay/dre_cleartemp git git@bitbucket.org:hidebay/dre_cleartmp.git
```

Anschließend den id.rsa.pub Key vom Server auslesen und im Repository bei Bitbucket
hinterlegen.
```php
cat ~/.ssh/id_rsa.pub
```
Nun Composer mitteilen dass er das Repository in den Update Zyklus aufnehmen soll

```php
composer require bender/dre_ClearTemp
```

fertig.
